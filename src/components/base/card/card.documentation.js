import * as description from './card.md';

export default {
  followsDesignSystem: true,
  description,
};
